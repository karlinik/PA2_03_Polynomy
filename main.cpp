#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <complex>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

ios_base & dummy_polynomial_manipulator(ios_base & x)
{
	return x;
}

ios_base & (*(polynomial_variable(const string & varName))) (ios_base & x) //meni promennou polynomu 
{
	return dummy_polynomial_manipulator;
}
class CPolynomial
{
private:
	vector<double>m_coef;
public:
	CPolynomial() { m_coef.push_back(0); } //OKimplicitni konstruktor, inicializuje polynom 0 - mozno pridat kopirujici konstruktor a destruktor
	double& operator [] (size_t x);//umozni nastavit/zjistit hodnotu koeficientu u zadane mocniny polynomu. Cteci varianta musi fungovat i pro konstantni polynomy
	double operator [] (size_t x) const;
	CPolynomial operator * (const CPolynomial &x)const ; //OKnasobeni polynomu desetinnym cislem nebo druhym polynomem
	CPolynomial operator * (double x) const ; //OK
	CPolynomial operator + (const CPolynomial &x) const;
	CPolynomial operator - (const CPolynomial &x) const;	//OKodecitani dvou polynomu
	bool operator ==	(const CPolynomial &x) const ; //OKporovnani polynomu na presnou shodu
	bool operator !=	(const CPolynomial &x) const ; //OKporovnani polynomu na presnou shodu
	double operator ()	(double x) const;//vyhodnoceni polynomu pro hodnotu x
	unsigned int Degree() const;
	friend ostream& operator << (ostream &out, const CPolynomial &x);

	vector<double> getCoef() const;
};
//-------------------------------------------------------------
ostream& operator << (ostream &out, const CPolynomial &x) {
	bool first = true;
	if (x.Degree() == 0)
		out << "0";
	else
	{
		for (int i = (x.m_coef.size()) - 1; i >= 0; i--) 
		{
			if (first == true)
			{
				if (x.m_coef[i] == 0)
					continue;
				else if (x.m_coef[i] < 0)
				{
					out << "- ";
					if (x.m_coef[i] == -1)
						out << "x^" << i;
					else
						out << (-1)*x.m_coef[i] << "*x^" << i;
				}
				else
				{
					if (x.m_coef[i] == 1)
						out << "x^" << i;
					else
						out << x.m_coef[i] << "*x^" << i;
				}
				first = false;
			}
			else
			{
				if (x.m_coef[i] < 0)
				{
					out << " - ";
					if (i == 0)
					{
						out << (-1)*x.m_coef[i];
					}
					else if (x.m_coef[i] == 1)
					{
						out << "x^" << i;
					}
					else
					{
						out << (-1)*x.m_coef[i] << "*x^" << i;
					}
				}
				else if (x.m_coef[i] > 0)
				{
					out << " + ";
					if (i == 0)
					{
						out << x.m_coef[i];
					}
					else if (x.m_coef[i] == 1)
					{
						out << "x^" << i;
					}
					else
					{
						out << x.m_coef[i] << "*x^" << i;
					}
				}
			}
		}
	}
	return out;
}
//-------------------------------------------------------------
CPolynomial CPolynomial::operator + (const CPolynomial &x) const {
	CPolynomial res;
	size_t size;
	
	if (m_coef.size() < x.m_coef.size())
		size = x.m_coef.size();
	else size = m_coef.size();

	res.m_coef.resize(size,0);

	for (size_t i = 0; i < size; i++)
	{
		if (i>=m_coef.size())
			res.m_coef[i] =x.m_coef[i];
		else if (i>=x.m_coef.size())
			res.m_coef[i] = m_coef[i];
		else
			res.m_coef[i] = (m_coef[i]+ x.m_coef[i]);
	}
	return (res);
}
//-------------------------------------------------------------
CPolynomial CPolynomial::operator - (const CPolynomial &x) const{
	CPolynomial res;
	size_t size;

	if (x.m_coef.size() < m_coef.size())
		size = m_coef.size();
	else size = x.m_coef.size();

	res.m_coef.resize(size,0);

	for (size_t i = 0; i < size; i++)
	{
		if (i>= m_coef.size())
			res.m_coef[i] = -x.m_coef[i];
		else if (i>= x.m_coef.size())
			res.m_coef[i] = m_coef[i];
		else
			res.m_coef[i] = m_coef[i] - x.m_coef[i];
	}
	return res;
}
//-------------------------------------------------------------
CPolynomial CPolynomial::operator * (double x) const{
	CPolynomial res;
	size_t size = m_coef.size();
	res.m_coef.resize(size,0);
	for (size_t i = 0; i < m_coef.size(); i++)
	{
		res.m_coef[i] = m_coef[i] * x;
	}
	return (res);
}
//-------------------------------------------------------------
CPolynomial CPolynomial::operator * (const CPolynomial &x) const {
	CPolynomial res;
	int size;
	size = m_coef.size() + x.m_coef.size();

	res.m_coef.resize(size,0);

	for (size_t i = 0; i < m_coef.size(); i++)
	{
		for (size_t j = 0; j < x.m_coef.size(); j++)
		{
			res.m_coef[i + j] += m_coef[i] * x.m_coef[j];
		}
	}
	return res;
}
//-------------------------------------------------------------
bool CPolynomial::operator == (const CPolynomial &x) const {
	if (this->Degree() == x.Degree())
	{
		for (size_t i = 0; i <= this->Degree(); i++)
			if (m_coef[i] != x.m_coef[i])
				return false;
		return true;
	}
	else return false;
}
//-------------------------------------------------------------
bool CPolynomial::operator != (const CPolynomial &x) const {
	return !((*this) == x);
}
//-------------------------------------------------------------
double& CPolynomial::operator [] (size_t x) {
	if (x < 0 || x >= m_coef.size())
		m_coef.resize(x + 1,0);
	return m_coef[x];
}
//-------------------------------------------------------------
double CPolynomial::operator [] (size_t x) const {
	if (x < 0 || x >= m_coef.size())
		return 0;
	return m_coef[x];
}
//-------------------------------------------------------------
double CPolynomial::operator () (double x) const{
	//pow
	double res=0;
	for (size_t i = 0; i <= this->Degree(); i++)
	{
		res += pow(x, i)*m_coef[i];
	}
	return res;
}
//-------------------------------------------------------------
unsigned int CPolynomial::Degree() const{
	if (m_coef.size() == 0)
		return 0;
	else {
		for (int i = (m_coef.size()) - 1; i >= 0; i--)
		{
			if (m_coef[i] != 0)
				return i;
		}
		return 0;
	}
}
//-------------------------------------------------------------
vector<double> CPolynomial::getCoef() const{
	vector<double> res;
	for (size_t i = 0; i <= this->Degree(); i++)
	{
		res.push_back(m_coef[i]);
		//cout << res[i]<<" ";
	}
	//cout << endl;
	return res;
}
//-------------------------------------------------------------
#ifndef __PROGTEST__
bool smallDiff(double a, double b)
{
	if ((a-b)==0)
		return true;
	else return false;
}
//-------------------------------------------------------------
bool dumpMatch(const CPolynomial & x, const vector<double> & ref)
{
	vector<double>tmp = x.getCoef();
	if (tmp.size() != ref.size())
	{
		return false;
		//cout << "vektory maji jinou velikost\n";
	}
	for (size_t i = 0; i < tmp.size(); i++)
	{
		if (tmp[i] != ref[i])
		{
			return false;
			//cout << "Odlisny prvek na pozici " << i << " ... " << tmp[i] << "!=" << ref[i] << endl;
		}
	}
	//cout << "vracim true\n";
	return true;
}
//-------------------------------------------------------------

int main(void)
{
	CPolynomial a, b, c;
	ostringstream out;

	a[0] = -10;
	a[1] = 3.5;
	a[3] = 1;
	
	assert(smallDiff(a(2), 5));
	out.str("");
	out << a;
	//cout << out.str() << endl;
	assert(out.str() == "x^3 + 3.5*x^1 - 10");
	a = a * -2;
	//a.getCoef();
	//dumpMatch(a, vector<double>{ 20.0, -7.0, -0.0, -2.0 });
	//cout << a.Degree() << endl;
	assert(a.Degree() == 3 && dumpMatch(a, vector<double>{ 20.0, -7.0, -0.0, -2.0 }));

	out.str("");
	out << a;
	assert(out.str() == "- 2*x^3 - 7*x^1 + 20");
	out.str("");
	out << b;
	assert(out.str() == "0");
	b[5] = -1;
	out.str("");
	out << b;
	assert(out.str() == "- x^5");
	
	c = a + b;
	assert(c.Degree() == 5
		&& dumpMatch(c, vector<double>{ 20.0, -7.0, 0.0, -2.0, 0.0, -1.0 }));

	out.str("");
	out << c;
	assert(out.str() == "- x^5 - 2*x^3 - 7*x^1 + 20");
	c = a - b;
	assert(c.Degree() == 5
		&& dumpMatch(c, vector<double>{ 20.0, -7.0, -0.0, -2.0, -0.0, 1.0 }));

	out.str("");
	out << c;
	assert(out.str() == "x^5 - 2*x^3 - 7*x^1 + 20");
	c = a * b;
	assert(c.Degree() == 8
		&& dumpMatch(c, vector<double>{ 0.0, -0.0, 0.0, -0.0, -0.0, -20.0, 7.0, 0.0, 2.0 }));

	out.str("");
	out << c;
	assert(out.str() == "2*x^8 + 7*x^6 - 20*x^5");
	assert(a != b);
	
	b[5] = 0;
	assert(!(a == b));
	a = a * 0;
	assert(a.Degree() == 0 && dumpMatch(a, vector<double>{ 0.0 }));
	out.str("");
	out << a;
	//cout <<"a"<< out.str()<<endl;
	out.str("");
	out << b;
	//cout << "b"<<out.str()<<endl;
	assert(a == b);
	//cout << b[100] << endl;
	// bonus
	/*a[2] = 4;
	a[1] = -3;
	b[3] = 7;
	out.str("");
	out << polynomial_variable("y") << "a=" << a << ", b=" << b;
	assert(out.str() == "a=4*y^2 - 3*y^1, b=7*y^3");

	out.str("");
	out << polynomial_variable("test") << c;
	assert(out.str() == "2*test^8 + 7*test^6 - 20*test^5");*/
	//cout << "proslo\n";
	//system("pause");
	return 0;
}
#endif /* __PROGTEST__ */
